{-# OPTIONS_GHC -fwarn-missing-signatures -funbox-strict-fields #-}
{-# LANGUAGE
    MultiWayIf, GeneralizedNewtypeDeriving, LiberalTypeSynonyms,
    ExistentialQuantification, ScopedTypeVariables, NamedFieldPuns #-}

module Main where
import Network.MateLight.Simple as MateLight

import Prelude hiding (id, (.), zip, zipWith, zipWith3, unzip)
import Data.Maybe
import Data.List
import Control.Category
import qualified Network.Socket as Sock

type Point2 = (Int, Int)

compose :: (Category cat) => [cat a a] -> cat a a
compose = foldr (.) id

mapIndexed :: (Int -> a -> b) -> [a] -> [b]
mapIndexed f = zipWith f [0..]

-- global vars
screenDimensions :: (Int, Int)
screenDimensions = (30, 12)

laserMoveDelayMs :: Int
laserMoveDelayMs = 1000

invandersMoveYDelayMs :: Int
invandersMoveYDelayMs = tickLength*100

tickLength :: Int
tickLength = 33000

initialState :: Model
initialState = Model
    { playerPos = (15,11)
    , laserPos = []
    , invaderPos = createInvaders
    , ticksSinceLastLaserMove = 0
    , ticksSinceLastInvadersXMove = 0
    , ticksSinceLastInvadersYMove = 0
    }

-- UI
---------------------------------------------------------------------------

mapPixels ::
    ListFrame ->          -- pixel array before
    (Point2 -> Pixel -> Pixel) ->
    ListFrame             -- pixel array after

mapPixels (ListFrame pixels) f =  ListFrame $
    mapIndexed
        (\y pixelRow -> mapIndexed
            (\x pixel -> f (x,y) pixel)
            pixelRow)
        pixels

-- create black background
createBackground :: ListFrame
createBackground = ListFrame $
    replicate (snd screenDimensions) $
        replicate (fst screenDimensions) (Pixel 0x00 0x00 0x00)

-- create invaders
createInvaders :: [Point2]
createInvaders = [(x, 0) | x <- [1,4..22]] ++ [(x, 2) | x <- [2,5..21]]

drawPlayer :: Renderer
drawPlayer state pixels = mapPixels pixels $
    \currentPixelPos currentPixel -> if
        | isShipPixel currentPixelPos -> Pixel 0x00 0x00 0xFF
        | otherwise                   -> currentPixel 
        where
            isShipPixel (x,y)
                | (x,y)   == playerPos state = True
                | (x+1,y) == playerPos state = True
                | (x-1,y) == playerPos state = True
                | (x,y+1) == playerPos state = True
                | otherwise                  = False

drawLaser :: Renderer
drawLaser state pixels = mapPixels pixels $
    \currentPixelPos currentPixel -> 
        if any (== currentPixelPos) $ laserPos state
        then Pixel 0x00 0xFF 0x00
        else currentPixel

drawInvader :: Renderer
drawInvader state pixels = mapPixels pixels $
    \currentPixelPos currentPixel ->
        if any (== currentPixelPos) $ invaderPos state
        then Pixel 0xFF 0x00 0x00
        else currentPixel
            

-- business logic
----------------------------------------------------------------------------
data Model = Model
    { playerPos :: !Point2
    , laserPos :: ![Point2]
    , invaderPos :: [Point2]
    , ticksSinceLastLaserMove :: Int
    , ticksSinceLastInvadersXMove :: Int
    , ticksSinceLastInvadersYMove :: Int
    } deriving (Show, Read, Eq)

handlePlayerMove :: Reducer
handlePlayerMove (Event "KEYBOARD" "\"a\"") state =
    if (fst $ playerPos state) > 1
    then state { playerPos = newPos }
    else state
    where newPos = ((fst $ playerPos state) - 1, snd $ playerPos state) -- move player to left

handlePlayerMove (Event "KEYBOARD" "\"d\"") state =
    if (fst $ playerPos state) < (fst $ screenDimensions)-2
    then state { playerPos = newPos }
    else state
    where newPos = ((fst $ playerPos state) + 1, snd $ playerPos state) -- move player to right

handlePlayerMove _ state = state

handlePlayerFire :: Reducer
handlePlayerFire (Event "KEYBOARD" "\"l\"") state = state { laserPos = newLaser : (laserPos state) }
    where newLaser = (fst $ playerPos state, (snd $ playerPos state) - 3)

handlePlayerFire _ state = state

handleReset :: Reducer
handleReset (Event "KEYBOARD" "\"r\"") state = initialState

handleReset _ state = state

moveLasers :: Reducer
moveLasers (Event "TICK" _) state = 
  if (ticksSinceLastLaserMove state) * tickLength >= laserMoveDelayMs 
  then state 
          { laserPos = (map (\(x, y) -> (x, y-1))) . (filter (\(x, y) -> y > 0)) $ laserPos state 
          , ticksSinceLastLaserMove = 0 
          }
  else state { ticksSinceLastLaserMove = (ticksSinceLastLaserMove state) + 1 }

moveLasers _ state = state

moveInvadersY :: Reducer
moveInvadersY (Event "TICK" _) state =
  if(ticksSinceLastInvadersYMove state) * tickLength >= invandersMoveYDelayMs
  then state
          { invaderPos = (map (\(x, y) -> (x, y+1))) . (filter (\(x, y) -> y <10)) $ invaderPos state
          , ticksSinceLastInvadersYMove = 0
          }
  else state { ticksSinceLastInvadersYMove = (ticksSinceLastInvadersYMove state) +1 }

moveInvadersY _ state = state

handleHit :: Reducer
handleHit _ state = state
    { invaderPos = (invaderPos state) \\ (laserPos state)
    , laserPos = (laserPos state) \\ (invaderPos state)
    }

-- boilerplate
-------------------------------------------------------------------------------------------------
type Renderer = Model -> ListFrame -> ListFrame
render :: Model -> ListFrame
render state = compose (map ($ state) renderers) $ createBackground
    where renderers = [drawPlayer, drawLaser, drawInvader]

type Reducer = Event String -> Model -> Model
reduce :: Reducer
reduce event state = compose (map ($ event) reducers) state
    where reducers = [handleReset, handlePlayerFire, handlePlayerMove, moveLasers, moveInvadersY, handleHit]

{-type Middleware a = Event a -> Model -> IO Event a
intercept :: Middleware a
intercept ev model = return ev-}

main :: IO ()
main = Sock.withSocketsDo $ runMate config programLoop initialState
    where
        programLoop :: [Event String] -> Model -> (ListFrame, Model)
        programLoop events oldState = let newState = (foldr reduce oldState $ (Event "TICK" "" : events)) in
            (render newState, newState)

        config = Config
            { cAddr = fromJust $ parseAddress "127.0.0.1"
            , cPort = 1337
            , cDimension = screenDimensions
            , cStepTime = Just tickLength
            , cSynchronized = False
            , cEventProviders = []
            }
