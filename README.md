# FritzLight Project

## Concept 
 
SpaceInvader-like game:

- User can move ship and fire with keyboard
- Enemy ships disappear when hit
- enemy ships move horizontally with every frame 
- enemy vertical movement speed (difficulty) can be set via command line
- game over if enemy ship reaches player ship


## Architecture
 
Application will be designed with a redux/elm-inspired functional architecture, consisting of 5 basic parts:

- Event Types:
    - Events are types that represent an action or event that happened somewhere in the program. They may contain data

- Model:
    - the model is a data type that represents the domain model and current program state

- Renders:
    - renderers are pure functions of the type `ListFrame -> Model -> ListFrame`
    - they render the application state (= update the UI)

- Interceptors:
    - interceptors are functions of the type `Event -> Model -> IO Event`
    - they can intercept and change an event before it reaches the reducers
    - interceptors can be used for logging and async tasks
    
- Reducers:
    - refucers are pure functions of the type `Model -> Event -> Model`
    - they contain the programs main business logic and can update the program state depending on the event that happened
    - reducers are easily composable